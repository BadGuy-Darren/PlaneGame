# coding = utf-8

import sys
from planeSprite.sprites import *


class LoadingScreen(object):
    """ 显示加载页面多线程 """

    def __init__(self):

        # self.screen = pygame.display.set_mode(SCREEN_RECT.size)

        self.load_end = False

        self.clock = pygame.time.Clock()

        self.bg = pygame.image.load("./images/bg/bg_black.jpg")

        load = Loading()
        self.load_group = pygame.sprite.Group(load)

    def loading(self, screen):

        while True:

            self.clock.tick(FRAME_PER_SEC)

            screen.blit(self.bg, (0, 0))

            self.load_group.update()
            self.load_group.draw(screen)

            pygame.display.update()

            if self.load_end:
                break


class BgMusic(object):
    """ 背景音樂 """

    def __init__(self):

        pygame.mixer.music.load("./musics/4uasf-1ckad.ogg")
        pygame.mixer.music.set_volume(0.5)

    def music_on(self):

        pygame.mixer.music.play(-1)
