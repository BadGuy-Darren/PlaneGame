# coding = utf-8
import pygame
from planeSprite.PlaneSprite import *


def fly():
    pygame.init()
    screen = pygame.display.set_mode((600, 900))
    bg = pygame.image.load("./images/bg/bg0.jpg")
    hero = pygame.image.load("./images/hero/hero01.png")
    screen.blit(bg, (0, 0))
    screen.blit(hero, (300, 900))
    hero_rect = pygame.Rect(300, 900, 100, 100)
    pygame.display.update()
    enemy = EnemySprite("./images/enemy/enemy0.png", 2)
    enemy_group = pygame.sprite.Group(enemy)
    clock = pygame.time.Clock()
    i = 0
    while True:
        event = pygame.event.poll()
        # if event:
        #     print(event)
        # if event.type == pygame.MOUSEBUTTONUP:

        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if hero_rect.y <= 0:
            hero_rect.y = 900
        else:
            hero_rect.y -= 10
        screen.blit(bg, (0, 0))
        screen.blit(hero, hero_rect)
        enemy_group.update()
        # print("enenmy, y=" + str(enemy.rect.y))
        enemy_group.draw(screen)
        pygame.display.update()


if __name__ == '__main__':
    fly()
