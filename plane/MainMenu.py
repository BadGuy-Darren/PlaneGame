# coding = utf-8

from ThePlaneWar import *
import images


class Menu(object):
    """ 首页菜单项 """

    def __init__(self):
        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption("Plane War")

        icon = pygame.image.load("images/image.ico")
        pygame.display.set_icon(icon)

        # 1.加载游戏窗口
        self.screen = pygame.display.set_mode(SCREEN_RECT.size)
        # 2.创建时间时钟
        self.clock = pygame.time.Clock()
        # 3.创建精灵
        self.__create_sprite()

    def menu_show(self):
        while True:
            # 1.刷新帧率
            self.clock.tick(30)
            # 2.事件监听
            self.__event_heard()
            # 3.更新绘制精灵
            self.__update_sprite()

            # 4.更新显示
            pygame.display.update()

    def __event_heard(self):
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                Menu.__game_over()

            # 鼠标按下，让状态变成可以移动
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                if (self.b_start.rect.left <= x <= self.b_start.rect.right) and (self.b_start.rect.top <= y <= self.b_start.rect.bottom):
                    self.b_start.pressed_on()
                elif (self.b_exit.rect.left <= x <= self.b_exit.rect.right) and (self.b_exit.rect.top <= y <= self.b_exit.rect.bottom):
                    self.b_exit.pressed_on()

            # 鼠标弹起，让状态变成不可以移动
            elif event.type == pygame.MOUSEBUTTONUP:

                x, y = pygame.mouse.get_pos()

                self.b_start.pressed_up()
                self.b_exit.pressed_up()

                if (self.b_start.rect.left <= x <= self.b_start.rect.right) and (self.b_start.rect.top <= y <= self.b_start.rect.bottom):
                    self.start_game()
                elif (self.b_exit.rect.left <= x <= self.b_exit.rect.right) and (self.b_exit.rect.top <= y <= self.b_exit.rect.bottom):
                    self.__game_over()

    def __create_sprite(self):
        """ 创建精灵组 """

        # 1.背景精灵组
        bg = BgSprite()
        bg.image = pygame.image.load("./images/bg/menu.jpg")
        bg.y_speed = 0
        self.back_group = pygame.sprite.Group(bg)

        # 2.创建狗头
        dh = DogHead()
        self.dhs = pygame.sprite.Group(dh)

        # 3.开始按钮
        self.b_start = Start()
        self.start = pygame.sprite.Group(self.b_start)

        # 4.退出按钮
        self.b_exit = ExitGame()
        self.exit_group = pygame.sprite.Group(self.b_exit)

        # 4.退出按钮
        self.game_title = Title()
        self.game_title_group = pygame.sprite.Group(self.game_title)

    def __update_sprite(self):
        # 1.更新背景
        self.back_group.update()
        self.back_group.draw(self.screen)
        # 2.绘制狗头
        self.dhs.draw(self.screen)
        # 3.绘制开始按钮
        self.start.draw(self.screen)
        # 4.绘制退出按钮
        self.exit_group.draw(self.screen)
        # 5.绘制标题
        self.game_title_group.draw(self.screen)

    @staticmethod
    def __game_over():

        pygame.quit()
        exit("退出游戏")

    @staticmethod
    def start_game():

        plane_game = PlaneWar()
        plane_game.game_start()


if __name__ == '__main__':
    menu = Menu()
    menu.menu_show()
