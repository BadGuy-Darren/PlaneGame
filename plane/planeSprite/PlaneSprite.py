# coding = utf-8

import pygame


"""飛機大戰遊戲精靈"""


class HeroSprite(pygame.sprite.Sprite):
    '''英雄飛機精靈'''
    def __init__(self, image_path, speed=1):
        super().__init__()

    def update(self):
        pass


class EnemySprite(pygame.sprite.Sprite):
    '''敵機精靈'''
    def __init__(self, image_path, speed=0.1):
        super().__init__()
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()
        self.speed = speed
        self.a = 0.1

    def update(self):
        if self.rect.y >= 900:
            self.rect.y = -10
        else:
            self.rect.y += self.speed
            self.speed += self.a