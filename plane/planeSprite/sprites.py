# coding = utf-8

import random
import pygame

SCREEN_RECT = pygame.Rect(0, 0, 512, 768)

FRAME_PER_SEC = 60
# 时间定时器常量
CREATE_ENEMY_EVENT = pygame.USEREVENT
# 攻击事件
HERO_FIRE_EVENT = pygame.USEREVENT + 1


class GameSprite(pygame.sprite.Sprite):
    """ 继承sprite后使用的参数名是有约束的，不可随意改变 """
    def __init__(self, image_path, x_speed=0.00, y_speed=1.00):
        super().__init__()
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()
        self.y_speed = y_speed
        self.x_speed = x_speed

    def update(self):
        self.rect.y += self.y_speed
        self.rect.x += self.x_speed


class BgSprite(GameSprite):

    def __init__(self, is_alt=False):
        super().__init__("./images/bg/bg2.jpg")
        if is_alt:
            self.rect.y = -self.rect.height

    def update(self):
        super().update()
        if self.rect.y >= SCREEN_RECT.height:
            self.rect.y = -self.rect.height


class EnemySprite(GameSprite):
    """ 敌机精灵 """

    def __init__(self):

        # 随机数拼接敌机
        enemy_directory = "./images/enemy/"
        self.enemy_num = random.randint(0, 7)
        enemy_name = "enemy" + str(self.enemy_num) + ".png"
        enemy_path = enemy_directory + enemy_name
        super().__init__(enemy_path)

        # 指定敌机随机速度
        self.y_speed = random.randint(4, 8)

        # 指定敌机随机位置
        self.rect.bottom = 0
        max_x = SCREEN_RECT.width - self.rect.width
        self.rect.x = random.randint(0, max_x)

    def update(self):
        super().update()

        # 判断敌机是否飞出屏幕，是则从精灵组删除
        if self.rect.y >= SCREEN_RECT.height:
            self.kill()


class HeroSprite(GameSprite):
    """ 英雄精灵 """

    def __init__(self):
        super().__init__("./images/hero/hero_b_1.png", 0, 0)

        # 1.初始位置
        self.rect.centerx = SCREEN_RECT.centerx
        self.rect.bottom = SCREEN_RECT.bottom - 120

        # 2.子弹精灵组
        self.bullets = pygame.sprite.Group()

        # 3.是否存活
        self.is_live = False

        # 4.等级
        self.level = 1

        # 5.英雄形态持续帧数
        self.state_time = 20 * FRAME_PER_SEC
        self.continue_time = 0

        # 6.对应英雄等级的形态
        self.images = [pygame.image.load("./images/hero/hero_b_" + str(v) + ".png") for v in range(1, 5)]

        # 7.等級1、2子弹图片
        self.bullet_images = ["./images/bullet/bullet" + str(v) + ".png" for v in range(1, 3)]

        # 8.是否有护盾
        self.has_safe = False

        # 9.子彈速度
        self.bullets_speed = 16

    def update(self):

        super().update()

        # 边界检测
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_RECT.right:
            self.rect.right = SCREEN_RECT.right
        if self.rect.top < SCREEN_RECT.top:
            self.rect.top = SCREEN_RECT.top
        if self.rect.bottom > SCREEN_RECT.bottom:
            self.rect.bottom = SCREEN_RECT.bottom

        # 是否降级检测
        if self.continue_time >= self.state_time and self.level > 1:
            self.level -= 1
            self.image = self.images[self.level - 1]
            self.continue_time = 0

        elif self.level > 1:
            self.continue_time += 1

    def fire(self):

        # 1.創建基礎子彈精靈
        if self.level < 3:

            bullet = Bullet(self.bullet_images[self.level - 1], 0, -self.bullets_speed)

        else:

            bullet = Bullet(self.bullet_images[1], 0, -self.bullets_speed)

            # 2.添加三級附加子彈精靈
            bullet3_l = Bullet("./images/bullet/bullet3l.png", -self.bullets_speed/4, -self.bullets_speed)
            bullet3_l.rect.bottom = self.rect.y + 30
            bullet3_l.rect.centerx = self.rect.centerx - 30

            bullet3_r = Bullet("./images/bullet/bullet3r.png", self.bullets_speed/4, -self.bullets_speed)
            bullet3_r.rect.bottom = self.rect.y + 30
            bullet3_r.rect.centerx = self.rect.centerx + 30

            self.bullets.add(bullet3_l)
            self.bullets.add(bullet3_r)

            if self.level > 3:

                # 3.添加四級附加子彈精靈
                bullet4_l = Bullet("./images/bullet/bullet4l.png", -self.bullets_speed, -self.bullets_speed)
                bullet4_l.rect.bottom = self.rect.y + 40
                bullet4_l.rect.centerx = self.rect.centerx - 50

                bullet4_r = Bullet("./images/bullet/bullet4r.png", self.bullets_speed, -self.bullets_speed)
                bullet4_r.rect.bottom = self.rect.y + 40
                bullet4_r.rect.centerx = self.rect.centerx + 50

                self.bullets.add(bullet4_l)
                self.bullets.add(bullet4_r)

        # 1.1 基礎子弹初始位置
        bullet.rect.bottom = self.rect.y + 20
        bullet.rect.centerx = self.rect.centerx

        # 4.子弹精灵组
        self.bullets.add(bullet)

    def up_hero(self):

        if self.level < 4:

            self.level += 1
            self.image = self.images[self.level - 1]

        self.continue_time = 0


class Bullet(GameSprite):
    """ 子弹精灵 """

    def __init__(self, bullet_image, x_speed, y_speed):
        super().__init__(bullet_image, x_speed, y_speed)

    def update(self):

        super().update()

        # 判定子弹是否飞出屏幕
        if self.rect.top < -self.rect.height or self.rect.left < -self.rect.width \
                or self.rect.right > SCREEN_RECT.right + self.rect.width:

            self.kill()


class Safe(GameSprite):
    """ 护盾精灵 """

    def __init__(self):
        super().__init__("./images/hero/safe1.png", 0, 0)

        self.state_time = 15 * FRAME_PER_SEC
        self.broken = False

    def update(self):
        self.state_time -= 1
        if self.state_time <= 0:
            self.broken = True

    def reset(self):
        self.state_time = 15 * FRAME_PER_SEC


class Boom(pygame.sprite.Sprite):
    """ 爆炸特效精灵 """

    def __init__(self, rect, num):
        super().__init__()
        if num == 0:
            i = 1
            j = 7
        elif num == 6:
            i = 5
            j = 7
        else:
            i = 1
            j = 4
        self.images = [pygame.image.load("./images/boom/boom0" + str(v) + ".png") for v in range(i, j)]
        self.image = self.images[0]

        self.rect = self.image.get_rect()
        self.rect.x = rect.x
        self.rect.y = rect.y

        self.index = 0
        self.interval = 20
        self.interval_index = 0

    def update(self):
        self.interval_index += 1

        if self.interval_index >= self.interval:
            self.interval_index = 0
            self.index += 1

            # 爆炸是否加載完
            if self.index >= len(self.images):
                self.kill()
            else:
                self.image = self.images[self.index]


class Score(pygame.sprite.Sprite):
    """ 分数 """

    def __init__(self):
        super().__init__()

        self.font = pygame.font.SysFont(None, 48)
        self.score = 0
        self.image = self.font.render("SCORE : " + str(self.score), False, (255, 200, 10))
        self.rect = self.image.get_rect()

    def update(self):
        self.image = self.font.render("SCORE : " + str(self.score), False, (255, 200, 10))


class Article(GameSprite):
    """ 游戏道具 """

    def __init__(self, rect):

        # 随机物资
        self.images_path = ["./images/hero/article" + str(v) + ".png" for v in range(1, 3)]
        self.article_num = random.randint(0, 1)
        self.image_path = self.images_path[self.article_num]

        super().__init__(self.image_path, 0, -2)
        self.a = 0.2

        self.rect.x = rect.x
        self.rect.y = rect.y

    def update(self):

        if self.rect.bottom < SCREEN_RECT.bottom - 10:

            self.rect.y += self.y_speed

            if self.y_speed < 3:
                self.y_speed = self.y_speed + self.a


class StaticSprite(pygame.sprite.Sprite):
    """ 静态精灵参考类 """

    def __init__(self, image_path):
        super().__init__()
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()

    def update(self):
        pass


class DogHead(StaticSprite):
    """ 狗头 """

    def __init__(self):
        super().__init__("./images/enemy/1111.png")
        self.rect.top = SCREEN_RECT.top
        self.rect.centerx = SCREEN_RECT.centerx


class Start(StaticSprite):
    """ 开始按钮 """

    def __init__(self):
        super().__init__("./images/botton/start.png")
        self.rect.centery = SCREEN_RECT.centery + 100
        self.rect.centerx = SCREEN_RECT.centerx

    def pressed_on(self):
        self.image = pygame.image.load("./images/botton/start_on.png")

    def pressed_up(self):
        self.image = pygame.image.load("./images/botton/start.png")


class ExitGame(StaticSprite):
    """ 退出游戏 """

    def __init__(self):
        super().__init__("./images/botton/exit.png")
        self.rect.centery = SCREEN_RECT.centery + 220
        self.rect.centerx = SCREEN_RECT.centerx

    def pressed_on(self):
        self.image = pygame.image.load("./images/botton/exit_on.png")

    def pressed_up(self):
        self.image = pygame.image.load("./images/botton/exit.png")


class Title(StaticSprite):
    """ 游戏标题 """

    def __init__(self):
        super().__init__("./images/bg/title.png")

        self.rect = self.image.get_rect()
        self.rect.centerx = SCREEN_RECT.centerx
        self.rect.top = SCREEN_RECT.top + 40


class Loading(pygame.sprite.Sprite):
    """ 加载等待图示 """

    def __init__(self):
        super().__init__()
        self.images = [pygame.image.load("./images/load/load" + str(v) + ".png") for v in range(1, 4)]
        self.image = self.images[0]

        self.rect = self.image.get_rect()
        self.rect.center = SCREEN_RECT.center

        # 定时播放参数
        self.index = 0
        self.interval = 20
        self.interval_index = 0

    def update(self):
        self.interval_index += 1

        if self.interval_index >= self.interval:
            self.interval_index = 0
            self.index += 1

            # 播放结束时从头开始
            if self.index >= len(self.images):
                self.index = 0
                self.image = self.images[self.index]
            else:
                self.image = self.images[self.index]
