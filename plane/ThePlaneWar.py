# coding = utf-8

import inspect
import ctypes
import threading
from Threads.Threads import *


class PlaneWar(object):

    def __init__(self):
        """ 游戏初始化 """

        # pygame.init()
        pygame.mixer.init()

        # 1.加载游戏窗口
        self.screen = pygame.display.set_mode(SCREEN_RECT.size)

        # 0.创建多线程，显示加载界面
        ls = LoadingScreen()
        t = threading.Thread(target=ls.loading, args=(self.screen, ))
        t.setDaemon(True)
        t.start()

        # 2.初始化游戏时钟
        self.clock = pygame.time.Clock()

        # 3.创建精灵
        self.__create_sprites()

        # 4.创建敌机生成定时器
        pygame.time.set_timer(CREATE_ENEMY_EVENT, 200)

        # 5.创建英雄开火定时器
        pygame.time.set_timer(HERO_FIRE_EVENT, 150)

        # 6.英雄出现后敌机出现等待帧数
        self.wait_time = 0

        # 7.结束显示加载
        ls.load_end = True
        stop_thread(t)

        bg_music = BgMusic()
        t_music = threading.Thread(target=bg_music.music_on(), args=())
        t_music.setDaemon(True)
        t_music.start()

    def __create_sprites(self):
        """ 创建精灵组 """

        # 1.背景精灵组
        bg1 = BgSprite()
        bg2 = BgSprite(True)

        self.back_group = pygame.sprite.Group(bg1, bg2)

        # 2.敌机精灵组
        self.enemy_group = pygame.sprite.Group()

        # 3.英雄精灵
        self.hero = HeroSprite()
        self.hero.is_live = True
        self.hero_group = pygame.sprite.Group(self.hero)

        # 4.爆炸特效精靈組
        self.boom_group = pygame.sprite.Group()

        # 5.分数精灵
        # self.score = Score()
        # self.score_group = pygame.sprite.Group(self.score)

        # 6.物资精灵组
        self.article_group = pygame.sprite.Group()

        # 7.护盾精灵组
        self.safe_box = pygame.sprite.Group()

    def game_start(self):
        """ 启动游戏 """

        print("游戏开始")
        while True:

            # 1.设置刷新帧率
            self.clock.tick(FRAME_PER_SEC)
            # 2.事件监听
            self.__event_heard()
            # 3.碰撞检测
            self.__collision_detection()
            # 4.更新绘制精灵
            self.__update_sprite()
            # 5.更新显示
            pygame.display.update()

    def __event_heard(self):
        """ 事件监听 """

        # 事件监听
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                PlaneWar.__game_over()

            elif event.type == CREATE_ENEMY_EVENT and self.wait_time > FRAME_PER_SEC:
                enemy = EnemySprite()
                self.enemy_group.add(enemy)

            elif event.type == HERO_FIRE_EVENT and self.wait_time > FRAME_PER_SEC:

                if self.hero.is_live:
                    self.hero.fire()

        # 使用键盘提供的方法 - 按键元组(上下左右)
        keys_pressed = pygame.key.get_pressed()

        if keys_pressed[pygame.K_RIGHT]:
            self.hero.x_speed = 8
        elif keys_pressed[pygame.K_LEFT]:
            self.hero.x_speed = -8
        else:
            self.hero.x_speed = 0

        if keys_pressed[pygame.K_UP]:
            self.hero.y_speed = -8
        elif keys_pressed[pygame.K_DOWN]:
            self.hero.y_speed = 8
        else:
            self.hero.y_speed = 0

    def __collision_detection(self):
        """ 碰撞检测 """

        # 1.摧毁敌机
        enemies = pygame.sprite.groupcollide(self.enemy_group, self.hero.bullets, True, True)

        if len(enemies) > 0:

            for enemy in enemies:

                boom = Boom(enemy.rect, enemy.enemy_num)
                self.boom_group.add(boom)

                # 6号机机率掉落物资
                if enemy.enemy_num == 6:

                    # self.score.score += 10
                    random_num = random.randint(1, 6)

                    if random_num == 6:
                    # if True:

                        article = Article(enemy.rect)
                        self.article_group.add(article)

                else:
                    pass
                    # self.score.score += 1

        # 2.英雄碰撞
        damaged = pygame.sprite.spritecollide(self.hero, self.enemy_group, self.hero.is_live)

        if len(damaged) > 0 and self.hero.is_live:

            # 英雄牺牲
            self.hero.kill()
            self.hero.is_live = False

            # 播放爆炸动画
            boom = Boom(self.hero.rect, 0)
            self.boom_group.add(boom)

        # 3.物资碰撞检测
        arti = pygame.sprite.groupcollide(self.article_group, self.hero_group, True, False)

        if len(arti) > 0:

            for key in arti:

                if key.article_num == 0:

                    # 增加英雄护盾
                    if not self.hero.has_safe:
                        self.hero.has_safe = True
                        self.safe = Safe()
                        self.safe.rect.center = self.hero.rect.center
                        self.safe_box.add(self.safe)

                    else:
                        self.safe.reset()

                elif key.article_num == 1:

                    self.hero.up_hero()

        # 4.敌机与护盾碰撞
        enemies = pygame.sprite.groupcollide(self.enemy_group, self.safe_box, True, True)

        if len(enemies) > 0 and self.hero.has_safe:

            for enemy in enemies:

                self.safe.kill()
                self.hero.has_safe = False

                boom = Boom(enemy.rect, enemy.enemy_num)
                self.boom_group.add(boom)

                # 6号机机率掉落物资
                if enemy.enemy_num == 6:

                    # self.score.score += 10
                    random_num = random.randint(1, 6)

                    if random_num == 6:
                    # if True:
                        article = Article(enemy.rect)
                        self.article_group.add(article)

                # else:
                #
                #     self.score.score += 1

    def __update_sprite(self):
        """ 更新精灵 """

        # 1.背景更新
        self.back_group.update()
        self.back_group.draw(self.screen)

        # 2.英雄更新
        self.hero_group.update()
        self.hero_group.draw(self.screen)

        if self.wait_time > FRAME_PER_SEC:

            # 3.敌机更新
            self.enemy_group.update()
            self.enemy_group.draw(self.screen)

            # 4.子弹更新
            self.hero.bullets.update()
            self.hero.bullets.draw(self.screen)

            # 5.爆炸更新
            self.boom_group.update()
            self.boom_group.draw(self.screen)

            # 6.分数更新
            # self.score_group.update()
            # self.score_group.draw(self.screen)

            # 7.物资更新
            self.article_group.update()
            self.article_group.draw(self.screen)

            # 8.护盾更新
            if self.hero.has_safe:

                if self.safe.broken:
                    self.safe.kill()
                    self.hero.has_safe = False

                self.safe.rect.center = self.hero.rect.center

            self.safe_box.update()
            self.safe_box.draw(self.screen)

        else:

            self.wait_time += 1

    @staticmethod
    def __game_over():
        """ 游戏结束 """

        pygame.quit()
        exit("遊戲結束")


def _async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""

    tid = ctypes.c_long(tid)

    if not inspect.isclass(exctype):
        exctype = type(exctype)

    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))

    if res == 0:
        raise ValueError("invalid thread id")

    elif res != 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def stop_thread(thread):

    _async_raise(thread.ident, SystemExit)


if __name__ == '__main__':

    plane = PlaneWar()
    plane.game_start()
